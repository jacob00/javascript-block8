//bubble sort method

let sorter = (array, order) =>{
    if(order === undefined || order ==='ascending'){
        function ascending() {
            for(var i = 0; i < array.length; i++) {
                for(var j=0; j < array.length; j++) {
                    if(array[i] < array[j]) {
                        var temp = array[i];
                        array[i] = array[j];
                        array[j] = temp;        
                    }
                }
            }
        }
        ascending()
        return array;
    }
    else if (order === 'descending'){
        function descending() {
            for(var i = 0; i < array.length; i++) {
                for(var j=0; j < array.length; j++) {
                    if(array[i] > array[j]) {
                        var temp = array[i];
                        array[i] = array[j];
                        array[j] = temp;        
                    }
                }
            }
        }
        descending()
        return array;
    }
    else return `wrong order provided ${order} please provide us with ascending or descending order instructions`
}
module.exports ={
    sorter
}