var matchThemUp = (mName, fName) => {
    
    var femaleYes = 0,
        femaleNo = 0, 
        maleYes = 0,
        maleNo = 0;

    var malePets = prompt(`Do you like pets ${mName}?`)
    if(malePets === 'yes' || malePets === 'Yes'){
        maleYes += 1;
    }
    else if(malePets === 'no' || malePets === 'No'){
        maleNo += 1;
    }
    var femalePets = prompt(`Do you like pets ${fName}?`)
    if(femalePets=== 'yes' || femalePets==='Yes'){
        femaleYes +=1;
    }
    else if(femalePets=== 'no' || femalePets==='No'){
        femaleNo +=1;
    }
    var maleBeer = prompt(`Do you like beer ${mName}?`)
    if(maleBeer=== 'yes' || maleBeer==='Yes'){
        maleYes += 1;
    }
    else if(maleBeer=== 'no' || maleBeer==='No'){
        maleNo +=1;
    }
    var femaleBeer = prompt(`Do you like beer ${fName}?`)
    if(femaleBeer=== 'yes' || femaleBeer==='Yes'){
        femaleYes +=1;
    }
    else if(femaleBeer=== 'no' || femaleBeer==='No'){
        femaleNo +=1;
    }
    var maleBooks = prompt(`Do you like reading books ${mName}?`)
    if(maleBooks=== 'yes' || maleBooks==='Yes'){
        maleYes += 1;
    }
    else if(maleBooks=== 'no' || maleBooks==='No'){
        maleNo +=1; 
    }
    var femaleBooks = prompt(`Do you like reading books ${fName}?`)
    if(femaleBooks=== 'yes' || femaleBooks==='Yes'){
        femaleYes +=1;
    }
    else if(femaleBooks=== 'no' || femaleBooks==='No'){
        femaleNo +=1;
    }   
    var maleBike = prompt(`Do you enjoy riding a bike ${mName}?`)
    if(maleBike=== 'yes' || maleBike==='Yes'){
        maleYes += 1;
    }
    else if(maleBike=== 'no' || maleBike==='No'){
        maleNo +=1;
    }
    var femaleBike = prompt(`Do you enjoy riding a bike ${fName}?`)
    if(femaleBike=== 'yes' || femaleBike==='Yes'){
        femaleYes +=1;
    }
    else if(femaleBike=== 'no' || femaleBike==='No'){
        femaleNo +=1;
    }
    var maleMusic = prompt(`Do you like mainstream music the most ${mName}?`)
    if(maleMusic=== 'yes' || maleMusic==='Yes'){
        maleYes += 1;
    }
    else if(maleMusic=== 'no' || maleMusic==='No'){
        maleNo +=1;
    }
    var femaleMusic = prompt(`Do you like mainstream music the most ${fName}?`)
    if(femaleMusic=== 'yes' || femaleMusic==='Yes'){
        femaleYes +=1;
    }
    else if(femaleMusic=== 'no' || femaleMusic==='No'){
        femaleNo +=1;
    }

    var maleSum = maleNo + maleYes;
    var femaleSum = femaleNo + femaleYes;

    if (femaleSum > maleSum){
        alert(`${mName} did not answer every question, retry the test.`)
    }
    else if (femaleSum === maleSum){
        if (maleYes >= (maleSum*0.5) && femaleYes >= (femaleSum *0.5)) {
            alert('We have a match!')
        }
        else if (maleNo >= (maleSum*0.5) && femaleNo >= (femaleSum *0.5)) {
            alert('We have a match!')
        }
        else alert('No match found :(')
    }
    else if (femaleSum < maleSum){
        alert(`${fName} did not answer every question, retry the test.`)
    }
}