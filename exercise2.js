var arr =[
    {name:'mike', age:22},
    {name:'robert', age:12},
    {name:'roger', age:44},
    {name:'peter', age:28},
    {name:'Ralph', age:67}
    ] 

var recursive = (arr, number, newArr) => {
    if (number < arr.length){
        newArr.push([arr[number].name, arr[number].age])
        number++
        return newArr
    } 
    else return number
    
}

module.exports = {
    recursive
}