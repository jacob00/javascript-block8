let tally = (arr, action) => {
    let obj = {}
    arr.forEach(ele => {
        if(ele in obj){
            obj[ele]++
        }
        else obj[ele] = 1;
    });
    if (action === undefined || action !== 'arr'){
        return obj;
    }
    else {
        let arr2 = [];
        for(var key in obj){
            arr2.push([key, obj[key]])
        }
        return arr2;
    }
}

module.exports = {
    tally
}