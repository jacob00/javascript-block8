let tally = (arr) => {
    let obj = {}
    arr.forEach(ele => {
        if(ele in obj){
            obj[ele]++
        }
        else obj[ele] = 1;
    });
    return obj;
}
module.exports = {
    tally
}
